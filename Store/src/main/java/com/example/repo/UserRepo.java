package com.example.repo;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.example.model.User;

public interface UserRepo extends CrudRepository<User, Integer> {
	
	public List<User> findAll();
	
	public User findUserByUserId(int i);

	public User findByNameAndAge(String target, double target2);
	
	public User findByUserIdAndName(int i, String name);
	

}
