package com.example.repo;

import org.springframework.data.repository.CrudRepository;

import com.example.model.Home;

public interface HomeDao extends CrudRepository <Home, Integer> {
	
	
	public Home findByHomeId(int i);

}
