package com.example.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.model.User;
import com.example.repo.UserRepo;

@RestController
public class UserController {

	private UserRepo userRepo;
	
	
	public UserController() {
		
	}
	
	
	@Autowired
	public UserController(UserRepo userRepo) {

		this.userRepo = userRepo;
	}
	
	@GetMapping("/allUsers")
	public List<User> findAll(){
		return userRepo.findAll();
	}
	
	
	@PostMapping("/insertUser")
	public String insertUser(@RequestBody User user) {
		userRepo.save(user);
		return "Inserted User";
	}
	
	
	@GetMapping("/SpecificUser")
	public User findUser(@RequestParam("target")String target, @RequestParam("target2")double target2 ) {
		return userRepo.findByNameAndAge(target, target2);
	}
	
	
	@DeleteMapping("/DeleteUser")
	public String DeleteUser(@RequestParam("Id")int id) {
		
		userRepo.deleteById(id);
		return "delete user successful";
		
	}
	
	@PutMapping("/UpdateUser")
	public User UpdateUser(@RequestParam("Id")int id, @RequestBody com.example.model.UpdateUser upUser) {
		User use = userRepo.findUserByUserId(id);
		use.setName(upUser.getName());
		use.setAge(upUser.getAge());

		userRepo.save(use);
		
		return use;
		
	}
	
	
	///Practice Query Params
	/*
	 *@GetMapping("/user/{id}")
	 *public Page<Question> getAllQuestionsByUserId(Pageable pageable, @PathVariable int id)
	 */
	
	
	@GetMapping("/SpecificUser/{id}")
	public User GetUser(@PathVariable int id) {
		int i = id;
		return userRepo.findUserByUserId(i);
		
	}
	
	@GetMapping("/SpecificUser/{id}/{name}")
	public User GetUser(@PathVariable int id,@PathVariable String name) {
		int i = id;
		String findname = name;
		return userRepo.findByUserIdAndName(i, findname);
		
	}

//	@GetMapping("/SpecificUser/{id}/{name}/{idpurch}")
//	public User GetUser(@PathVariable int id,@PathVariable String name, @PathVariable int idpurch) {
//		int i = id;
//		String findname = name;
//		int ipurch = idpurch;
//		return userRepo.findByUserIdAndName(i, findname, ipurch);
//		
//	}

	
}
