package com.example.repo;

import org.springframework.data.repository.CrudRepository;

import com.example.model.Furniture;



public interface FurnitureDao extends CrudRepository<Furniture, Integer> {

	public Furniture findByFurnitureId(int i);
	
}
