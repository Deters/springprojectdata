package com.example.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="Home")
public class Home {
	
	

	@Id
	@Column(name="Home_Id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int homeId;
	
	
	@Column(name="Home_name")
	private String homeName;
	
	
	@Column(name="Home_size")
	private double homeSize;
	
	
	@OneToMany(mappedBy="home", fetch= FetchType.EAGER, cascade= CascadeType.ALL)
	List<Furniture> homeFurniture;
	
	
	public Home() {
		
	}


	public Home(int homeId, String homeName, double homeSize, List<Furniture> homeFurniture) {
		super();
		this.homeId = homeId;
		this.homeName = homeName;
		this.homeSize = homeSize;
		this.homeFurniture = homeFurniture;
	}


	public Home(String homeName, double homeSize, List<Furniture> homeFurniture) {
		super();
		this.homeName = homeName;
		this.homeSize = homeSize;
		this.homeFurniture = homeFurniture;
	}
	
	


	public Home(String homeName, double homeSize) {
		super();
		this.homeName = homeName;
		this.homeSize = homeSize;
	}


	public int getHomeId() {
		return homeId;
	}


	public void setHomeId(int homeId) {
		this.homeId = homeId;
	}


	public String getHomeName() {
		return homeName;
	}


	public void setHomeName(String homeName) {
		this.homeName = homeName;
	}


	public double getHomeSize() {
		return homeSize;
	}


	public void setHomeSize(double homeSize) {
		this.homeSize = homeSize;
	}


	public List<Furniture> getHomeFurniture() {
		return homeFurniture;
	}


	public void setHomeFurniture(List<Furniture> homeFurniture) {
		this.homeFurniture = homeFurniture;
	}


	@Override
	public String toString() {
		return "Home [homeId=" + homeId + ", homeName=" + homeName + ", homeSize=" + homeSize + ", homeFurniture="
				+ homeFurniture + "]";
	}

	

}
