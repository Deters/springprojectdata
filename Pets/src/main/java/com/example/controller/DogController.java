package com.example.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.model.Dog;
import com.example.repo.DogRepo;


@RestController
public class DogController {
	
	private DogRepo dogRepo;
	
	
	@GetMapping("/allPets")
	public List<Dog> findAll(){
	return dogRepo.findAll();
	}
	
	@PostMapping("/addPet")
	public String insertDog(@RequestBody Dog dog) {
		dogRepo.save(dog);
		//dogRepo.delete(dog);
		
		return "success";
	}
	
	
	public DogController() {
		
	}
	
	@Autowired
	public DogController(DogRepo dogRepo) {
		this.dogRepo = dogRepo;
	}
	

}
