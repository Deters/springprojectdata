package com.example.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="Users")
public class User {

	
	
	@Id
	@Column(name="User_Id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int userId;
	
	@Column(name="User_name")
	private String name;
	
	@Column(name="User_age")
	private double age;
	
	@OneToMany(mappedBy = "user", fetch = FetchType.EAGER)
	public List<Purchase> customerPurchases;
	
	
	public User() {
		
	}


	public User(int userId, String name, double age, List<Purchase> customerPurchases) {
		super();
		this.userId = userId;
		this.name = name;
		this.age = age;
		this.customerPurchases = customerPurchases;
	}


	public User(String name, double age, List<Purchase> customerPurchases) {
		super();
		this.name = name;
		this.age = age;
		this.customerPurchases = customerPurchases;
	}


	public int getUserId() {
		return userId;
	}


	public void setUserId(int userId) {
		this.userId = userId;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public double getAge() {
		return age;
	}


	public void setAge(double age) {
		this.age = age;
	}


	public List<Purchase> getCustomerPurchases() {
		return customerPurchases;
	}


	public void setCustomerPurchases(List<Purchase> customerPurchases) {
		this.customerPurchases = customerPurchases;
	}


	@Override
	public String toString() {
		return "User [userId=" + userId + ", name=" + name + ", age=" + age + ", customerPurchases=" + customerPurchases
				+ "]";
	}
	
	
	
}
