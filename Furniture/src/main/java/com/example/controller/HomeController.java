package com.example.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.model.Furniture;
import com.example.model.Home;
import com.example.repo.FurnitureDao;
import com.example.repo.HomeDao;
import com.example.service.FurnitureService;
import com.example.service.HomeService;

@RestController
@RequestMapping("/api")
public class HomeController {
	
	
	private HomeService homeService;
	private FurnitureService furnitureService;
	private HomeDao homeDao;
	private FurnitureDao furnDao;

	public HomeController() {
		super();
	}

	@Autowired
	public HomeController(HomeService homeService, FurnitureService furnitureService,
			HomeDao homeDao, FurnitureDao furnDao) {
		super();
		this.homeService = homeService;
		this.furnitureService = furnitureService;
		this.homeDao = homeDao;
		this.furnDao = furnDao;
	}
	
	@GetMapping("/AllHomes")
	public List<Home> findAll(){
		List<Home> allHomes = new ArrayList<>();
		allHomes = homeService.findAll();
		
		return allHomes;
		
	}
	@GetMapping("/AllFurniture")
	public List<Furniture> findAllFurn(){
		List<Furniture> allFurniture = new ArrayList<>();
		allFurniture = furnitureService.findAllFurnList();
		
		
		return allFurniture; 
		
	}
	
	@PostMapping("/NewHome")
	public String insertHome(@RequestBody Home home) {
		
		homeService.insert(home);
		
		
		return "successfully inputted new home" ;
		
	}
	
	
	
	@PostMapping("/NewFurniture")
	public String insertFurniture(@RequestBody Furniture furn) {
		furnDao.save(furn);
		return "Succesfully inputted furnature";
	}
	
	
	@PutMapping("/UpdateHome/{id}")
	public String updateHome(@PathVariable int id, @RequestBody Home home) {
		home.setHomeId(id);
		homeDao.save(home);
		return "successfully update home";
	}
	
	@PutMapping(path = "/UpdateFurniture/{furnid}/Home/{homeid}")
	public String updateFurn(@PathVariable int furnid,@PathVariable int homeid, @RequestBody Furniture furn) {
		furn.setFurnitureId(furnid);
		Home home = homeDao.findByHomeId(homeid);
		furn.setHome(home);
		furnDao.save(furn);
		return "Succesfully Updated Furnature";
		
	}
	
	@DeleteMapping(path = "/DeleteHome/{homeid}")
	public String DeleteHome(@PathVariable int homeid) {
		Home home = homeDao.findByHomeId(homeid);
		homeDao.delete(home);
		return "successfully deleted home";
		
	}
	
	@DeleteMapping(path = "/DeleteFurn/{furnid}")
	public String DeleteFurniture(@PathVariable int furnid) {
		Furniture furn = furnDao.findByFurnitureId(furnid);
		furnDao.delete(furn);
		return "successfully deleted furnature";
	}
	

}
