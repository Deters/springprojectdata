package com.example.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name="Furniture")
public class Furniture {
	
	@Id
	@Column(name="FurnitureId")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int furnitureId;
	
	
	@Column(name="Furniture_Name")
	private String furnitureName;
	
	
	@Column(name="Furniture_size")
	private double furnitureSize;
	
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="HomeKey")
	private Home home;
	
	public Furniture() {
		
	}

	public Furniture(String furnitureName, double furnitureSize, Home home) {
		super();
		this.furnitureName = furnitureName;
		this.furnitureSize = furnitureSize;
		this.home = home;
	}

	public Furniture(int furnitureId, String furnitureName, double furnitureSize, Home home) {
		super();
		this.furnitureId = furnitureId;
		this.furnitureName = furnitureName;
		this.furnitureSize = furnitureSize;
		this.home = home;
	}

	public int getFurnitureId() {
		return furnitureId;
	}

	public void setFurnitureId(int furnitureId) {
		this.furnitureId = furnitureId;
	}

	public String getFurnitureName() {
		return furnitureName;
	}

	public void setFurnitureName(String furnitureName) {
		this.furnitureName = furnitureName;
	}

	public double getFurnitureSize() {
		return furnitureSize;
	}

	public void setFurnitureSize(double furnitureSize) {
		this.furnitureSize = furnitureSize;
	}
	
	@JsonIgnore
	public Home getHome() {
		return home;
	}
	@JsonProperty
	public void setHome(Home home) {
		this.home = home;
	}

	@Override
	public String toString() {
		return "Furniture [furnitureId=" + furnitureId + ", furnitureName=" + furnitureName + ", furnitureSize="
				+ furnitureSize + ", home=" + home.getHomeName() + "]";
	}

	
	
}
