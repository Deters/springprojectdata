package com.example.repo;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.example.model.Dog;

public interface DogRepo extends CrudRepository<Dog,Integer>{

	public List<Dog> findAll();

	public Dog findByDogName(String name);
	public Dog findByAge(double age);
	

	
	
}
