package com.example.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.model.Furniture;
import com.example.repo.FurnitureDao;

@Service
public class FurnitureService {
	
	private FurnitureDao furnitureDao;

	public FurnitureService() {
		super();
	}

	@Autowired
	public FurnitureService(FurnitureDao furnitureDao) {
		super();
		this.furnitureDao = furnitureDao;
	}
	
	
	public List<Furniture> findAllFurnList(){
		return (List<Furniture>) furnitureDao.findAll();
		
	}
	
	

}
