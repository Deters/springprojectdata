package com.example.repo;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.example.model.Purchase;

public interface PurchaseRepo extends CrudRepository<Purchase, Integer> {
	
	public List<Purchase> findAll();
	
	public Purchase findByItemAndPrice(String name, double cost);
	
	public Purchase findByPurchaseId(int i);
	
	

}
