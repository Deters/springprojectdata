package com.example.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="PetTable")
public class Dog {
	
	@Id
	@Column(name="DogId")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int dogId;
	
	@Column(name="Name")
	private String dogName;
	
	@Column(name="age")
	private double age;
	
	public Dog() {
		
	}

	public int getDogId() {
		return dogId;
	}

	public void setDogId(int dogId) {
		this.dogId = dogId;
	}

	public String getDogName() {
		return dogName;
	}

	public void setDogName(String dogName) {
		this.dogName = dogName;
	}

	public double getAge() {
		return age;
	}

	public void setAge(double age) {
		this.age = age;
	}

	public Dog(int dogId, String dogName, double age) {
		super();
		this.dogId = dogId;
		this.dogName = dogName;
		this.age = age;
	}

	@Override
	public String toString() {
		return "Dog [dogId=" + dogId + ", dogName=" + dogName + ", age=" + age + "]";
	}
	
	
	
	
	
}