package com.example.model;

public class UpdateUser {
	
	private String name;
	private double age;
	
	public UpdateUser() {
		
	}

	public UpdateUser(String name, double age) {
		super();
		this.name = name;
		this.age = age;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getAge() {
		return age;
	}

	public void setAge(double age) {
		this.age = age;
	}

	
	
}
