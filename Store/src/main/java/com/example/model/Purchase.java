package com.example.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name="Purchases")
public class Purchase {
	
	@Id
	@Column(name="purchaseId")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int purchaseId;
	
	@Column(name="Item")
	private String item;
	
	@Column(name="price")
	private double price;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="user_FK")
	private User user;
	
	
	public Purchase() {
		
	}


	public Purchase(int purchaseId, String item, double price, User user) {
		super();
		this.purchaseId = purchaseId;
		this.item = item;
		this.price = price;
		this.user = user;
	}


	public Purchase(String item, double price, User user) {
		super();
		this.item = item;
		this.price = price;
		this.user = user;
	}


	public int getPurchaseId() {
		return purchaseId;
	}


	public void setPurchaseId(int purchaseId) {
		this.purchaseId = purchaseId;
	}


	public String getItem() {
		return item;
	}


	public void setItem(String item) {
		this.item = item;
	}


	public double getPrice() {
		return price;
	}


	public void setPrice(double price) {
		this.price = price;
	}

	@JsonIgnore
	public User getUser() {
		return user;
	}

	@JsonProperty
	public void setUser(User user) {
		this.user = user;
	}


	@Override
	public String toString() {
		return "Purchase [purchaseId=" + purchaseId + ", item=" + item + ", price=" + price + ", user=" + user.getName() + "]";
	}
	
	
	
	
	

}
