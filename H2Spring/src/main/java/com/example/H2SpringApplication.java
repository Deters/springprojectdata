package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class H2SpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(H2SpringApplication.class, args);
	}

}
