package com.example.repo;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.example.model.Food;


public interface FoodDao extends CrudRepository<Food,Integer>{

	
	public List<Food> findAll();
	public List<Food> findByOrderByDishName();
	public List<Food> findOrderByDishNameDesc();
	
	//public Food findByDishName(String dishName);
	public Food findByCalories(double lol);
	public Food findByDishNameAndCalories(String dishin, double heya);
	
}
