package com.example.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.model.Purchase;
import com.example.repo.PurchaseRepo;

@RestController
public class PurchaseController {

	private PurchaseRepo purchaseRepo;
	
	
	public PurchaseController() {
		
	}
	
	@Autowired
	public PurchaseController(PurchaseRepo purchaseRepo) {
		this.purchaseRepo = purchaseRepo;
		
	}
	
	
	@GetMapping("/allPurchases")
	public List<Purchase> findAll(){
		return purchaseRepo.findAll();
	}
	
	@PostMapping("/insertPurchase")
	public String insertPurchase(@RequestBody Purchase purchase) {
		purchaseRepo.save(purchase);
		
		return "inserted purchase";
	}
	
	
	@GetMapping("/GetSpecificPurchase")
	public Purchase getPurchase(@RequestParam("Name")String name, @RequestParam("Cost")double cost) {
		return purchaseRepo.findByItemAndPrice(name, cost);
	}
	
	
	@DeleteMapping("/DeletePurchase")
	public String DeletePurchase(@RequestParam("Id")int purchaseId) {
		purchaseRepo.deleteById(purchaseId);
		return "Deleted Successfully";
		
	}
	
	@GetMapping("/GetByPurchaseId/{id}")
	public Purchase getPurchaseById(@PathVariable int id) {
		return purchaseRepo.findByPurchaseId(id);
	}
	
	
}
