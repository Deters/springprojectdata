package com.example.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.model.Home;
import com.example.repo.HomeDao;

@Service
public class HomeService {
	
	
	private HomeDao homeDao;

	public HomeService() {
		super();
	}

	@Autowired
	public HomeService(HomeDao homeDao) {
		super();
		this.homeDao = homeDao;
	}
	
	
	public List<Home> findAll(){
		return (List<Home>) homeDao.findAll();
	}
	
	public void insert(Home home) {
		homeDao.save(home);
		
	}
	
	
	public String updateHome(int id, Home home) {
		Home updateHome = new Home();
		 updateHome = homeDao.findByHomeId(id);
		if(updateHome.equals(null) ) {
			return "Home Not Found";
		}else {
		homeDao.save(home);
			return null;
		}
	}
	
	

}
